<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="src/css/custom.css" type="text/css">

    <title>Laptop</title>
  </head>
  <body>
    <!-- Markup for header -->
    <?php
      include_once('views/frontend/element/header.php');
    ?>

    <!-- Markup for Single Product Section-->
    <div class="container" id="single-product">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="cardImg">
          <img class="card-img-top img-fluid" src="src/img/img-6.jpg">
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <h2>Mackbook Pro (2018)
      </h2>
      <p>*****  
        <u>(1 customer review)
        </u>
      </p>
      <p>
        <span>$45.00-$60.00
        </span>
      </p>
      <p>This is the product short description. You can choose which  columns display in the table. 
      </p>
      <div class="product_color">
        Color 
        <br>
        <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">
          Chose an option
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">Gray
          </a>
          <a class="dropdown-item" href="#">Golden
          </a>
          <a class="dropdown-item" href="#">Black
          </a>
          <a class="dropdown-item" href="#">White
          </a>
        </div>
      </div>
      <div class="product_size">
        <br>
        Size 
        <br>
        <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">
          Chose an option
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">Small
          </a>
          <a class="dropdown-item" href="#">Large
          </a>
        </div>
        <br>
        <br>
          <p class="price font-weight-bold"><del class="pr-2">$1000</del><span>$8000</span></p>
        <div class="cardHover">
            <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
            <button class="btn"><i class="fa fa-edit"></i></button>
            <button class="btn"><i class="fa fa-exchange"></i></button>
        </div>
  </div>
</div>
</div>
</div>


    
    <!-- Info Link Section -->
    <?php
      include_once('views/frontend/element/infoLinkSec.php');
    ?>

    <!-- Footer Link Section -->
    <?php
      include_once('views/frontend/element/footerLinkSec.php');
    ?>
  
    <!-- Copyright Text -->
    <?php
      include_once('views/frontend/element/copyrightTextSec.php');
    ?>
    
    <!-- Optional JavaScript -->
    <script src="src/js/jquery3.2.1.min.js"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/custom.js"></script>
  </body>
</html>
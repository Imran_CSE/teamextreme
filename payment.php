<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="src/css/custom.css" type="text/css">

    <title>Laptop</title>
  </head>
  <body>
    <!-- Markup for header -->
    <?php
      include_once('views/frontend/element/header.php');
    ?>

    <!-- Markup for Payment Option Section-->
    
    <div class="container" id="payment">
            <div class="form-group">
                    <h1> Payment Method</h1>
                </div>
                <form method="post" action="#">
                    <fieldset class="form-group">
                        <!-- Group of default radios - option 1 -->
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample1" name="groupOfDefaultRadios">
                                <label class="custom-control-label" for="defaultGroupExample1">Cheque / Bank Transfer</label>
                            </div>
                        </div>
                        <!-- Group of default radios - option 2 -->
                
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample2" name="groupOfDefaultRadios">
                                <label class="custom-control-label" for="defaultGroupExample2">Cash On Delivery</label>
                            </div>
                        </div>
                
                        <!-- Group of default radios - option 3 -->
                
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample3" name="groupOfDefaultRadios">
                                <label class="custom-control-label" for="defaultGroupExample3">Online Cards</label>
                            </div>
                        </div>
                
                        <!-- Group of default radios - option 4-->
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample4" name="groupOfDefaultRadios">
                                <label class="custom-control-label" for="defaultGroupExample4">Bkash / Mkash / Upay</label>
                            </div>
                        </div>
                
                    </fieldset>
                
                    <div class="form-group">
                        <h1>
                            Billing Address
                        </h1>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox">
                            <label class="form-check-label">
                
                                My billing and shipping address are the same
                            </label>
                        </div>
                    </div>
                    <div>
                      <a href="thanks.php" class="btn btn-success">Confirm</a>
                    </div>
                
                </form>
    </div>

    
    <!-- Info Link Section -->
    <?php
      include_once('views/frontend/element/infoLinkSec.php');
    ?>

    <!-- Footer Link Section -->
    <?php
      include_once('views/frontend/element/footerLinkSec.php');
    ?>
  
    <!-- Copyright Text -->
    <?php
      include_once('views/frontend/element/copyrightTextSec.php');
    ?>
    
    <!-- Optional JavaScript -->
    <script src="src/js/jquery3.2.1.min.js"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/custom.js"></script>
  </body>
</html>
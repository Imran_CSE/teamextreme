<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="src/css/custom.css" type="text/css">

    <title>Project by Team Extreme</title>
  </head>
  <body>
    <!-- Markup for header -->
    <?php
      include_once('views/frontend/element/header.php');
    ?>

    <!-- Markup for Slider -->
    <?php
      include_once('views/frontend/element/slider.php');
    ?>

    <!-- Markup for service Section-->
    <?php
      include_once('views/frontend/element/service.php');
    ?>

    <!-- Markup for Product Display Section-->
    <?php
      include_once('views/frontend/element/productDisplay.php');
    ?>

    <!-- Promotion Section -->
    <?php
      include_once('views/frontend/element/promotionSec.php');
    ?>

    <!-- Subscription Form -->
    <?php
      include_once('views/frontend/element/subscription.php');
    ?>
    <!-- Brand Section -->
    <?php
      include_once('views/frontend/element/brandSec.php');
    ?>
    <!-- Info Link Section -->
    <?php
      include_once('views/frontend/element/infoLinkSec.php');
    ?>

    <!-- Footer Link Section -->
    <?php
      include_once('views/frontend/element/footerLinkSec.php');
    ?>
  
    <!-- Copyright Text -->
    <?php
      include_once('views/frontend/element/copyrightTextSec.php');
    ?>
    
    <!-- Optional JavaScript -->
    <script src="src/js/jquery3.2.1.min.js"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/custom.js"></script>
  </body>
</html>
<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="src/css/custom.css" type="text/css">

    <title>All Product</title>
  </head>
  <body>
    <!-- Markup for header -->
    <?php
      include_once('views/frontend/element/header.php');
    ?>

    <!-- Markup for Product Category Section-->
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="singleProduct mt-5">
                    <div class="card">
                        <div class="cardImg">
                            <a href="single-product.php">
                                <img class="card-img-top img-fluid" src="src/img/desktop-2.jpg">
                            </a>
                        </div>
                        <div class="cardContent text-center">
                            <div class="card-body">
                                <h4 class="productTitle">
                                    <a href="">Apple Pc</a>
                                </h4>
                                <p class="price"><del>$1000</del><span>$8000</span></p>
                                <div class="productRating">
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star-half-o"></i></span>
                                </div>
                                <div class="cardHover">
                                    <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                                    <button class="btn"><i class="fa fa-edit"></i></button>
                                    <button class="btn"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="singleProduct mt-5">
                    <div class="card">
                        <div class="cardImg">
                            <a href="#">
                                <img class="card-img-top img-fluid" src="src/img/mobile-2.jpg">
                            </a>
                        </div>
                        <div class="cardContent text-center">
                            <div class="card-body">
                                <h4 class="productTitle">Smart Phone Primo V1</h4>
                                <p class="price"><del>$1000</del><span>$8000</span></p>
                                <div class="productRating">
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star-half-o"></i></span>
                                </div>
                                <div class="cardHover">
                                    <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                                    <button class="btn"><i class="fa fa-edit"></i></button>
                                    <button class="btn"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="singleProduct mt-5">
                    <div class="card">
                        <div class="cardImg">
                            <img class="card-img-top img-fluid" src="src/img/laptop-2.jpg">
                        </div>
                        <div class="cardContent text-center">
                            <div class="card-body">
                                <h4 class="productTitle">Asus TUF</h4>
                                <p class="price"><del>$1000</del><span>$8000</span></p>
                                <div class="productRating">
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star-half-o"></i></span>
                                </div>
                                <div class="cardHover">
                                    <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                                    <button class="btn"><i class="fa fa-edit"></i></button>
                                    <button class="btn"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="singleProduct mt-5">
                    <div class="card">
                        <div class="cardImg">
                            <img class="card-img-top img-fluid" src="src/img/mobile-1.jpg">
                        </div>
                        <div class="cardContent text-center">
                            <div class="card-body">
                                <h4 class="productTitle">Asus ZenPhone</h4>
                                <p class="price"><del>$1000</del><span>$8000</span></p>
                                <div class="productRating">
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star-half-o"></i></span>
                                </div>
                                <div class="cardHover">
                                    <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                                    <button class="btn"><i class="fa fa-edit"></i></button>
                                    <button class="btn"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="singleProduct mt-5">
                    <div class="card">
                        <div class="cardImg">
                            <img class="card-img-top img-fluid" src="src/img/desktop-1.jpg">
                        </div>
                        <div class="cardContent text-center">
                            <div class="card-body">
                                <h4 class="productTitle">Dell Brand Pc</h4>
                                <p class="price"><del>$1000</del><span>$8000</span></p>
                                <div class="productRating">
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star-half-o"></i></span>
                                </div>
                                <div class="cardHover">
                                    <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                                    <button class="btn"><i class="fa fa-edit"></i></button>
                                    <button class="btn"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="singleProduct mt-5">
                    <div class="card">
                        <div class="cardImg">
                            <img class="card-img-top img-fluid" src="src/img/laptop-1.jpg">
                        </div>
                        <div class="cardContent text-center">
                            <div class="card-body">
                                <h4 class="productTitle">Asus ROG Strix</h4>
                                <p class="price"><del>$1000</del><span>$8000</span></p>
                                <div class="productRating">
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star"></i></span>
                                    <span><i class="fa fa-star-half-o"></i></span>
                                </div>
                                <div class="cardHover">
                                    <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                                    <button class="btn"><i class="fa fa-edit"></i></button>
                                    <button class="btn"><i class="fa fa-exchange"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- Info Link Section -->
    <?php
      include_once('views/frontend/element/infoLinkSec.php');
    ?>

    <!-- Footer Link Section -->
    <?php
      include_once('views/frontend/element/footerLinkSec.php');
    ?>
  
    <!-- Copyright Text -->
    <?php
      include_once('views/frontend/element/copyrightTextSec.php');
    ?>
    
    <!-- Optional JavaScript -->
    <script src="src/js/jquery3.2.1.min.js"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/custom.js"></script>
  </body>
</html>
<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="src/css/custom.css" type="text/css">

    <title>Laptop</title>
  </head>
  <body>
    <!-- Markup for header -->
    <?php
      include_once('views/frontend/element/header.php');
    ?>

    <!-- Markup for Shipping Details Section-->
    
    <div class="container" id="shipping-details">
        <div class="form-group">
            <h1> Shipping Address</h1>
        </div>
        
        <form method="post" action="#">
            <fieldset class="form-group">
        
                <div class="form-group">
                    <label for="firstName">First Name:</label>
                    <input class="form-control" type="text" name="firstName" value="" id="firstName">
                </div>
        
                <div class="form-group">
                    <label for="lastName">Last Name:</label>
                    <input class="form-control" type="text" name="lastName" value="" id="lastName">
                </div>
        
                <div class="form-group">
                    <label for="company">Company:</label>
                    <input class="form-control" type="text" name="company" value="" id="company">
                </div>
        
                <div class="form-group">
                    <label for="street_address">Street Address:</label>
                    <input class="form-control" type="text" name="street_address" value="" id="street_address">
                </div>
        
                <div class="form-group">
                    <label for="city">City:</label>
                    <select class="form-control" id="city" name="city">
                        <option>Please Select a City</option>
                        <option value="dhaka">Dhaka</option>
                        <option value="chittagong">Chittagong</option>
                        <option value="sylhet">Sylhet</option>
                        <option value="barishal">Barishal</option>
                        <option value="rajshahi">Rajshahi</option>
                        <option value="rangpur">Rangpur</option>
                        <option value="khulna">Khulna</option>
                    </select>
                </div>
        
            </fieldset>
            <div class="form-group">
                <label for="area">Area:</label>
                <select class="form-control" id="area" name="area">
                    <option>Please Select a Area</option>
                    <option value="raojan">Raojan</option>
                    <option value="fatikchari">Fatikchari</option>
                    <option value="anowara">Anowara</option>
                    <option value="banskhali">Banskhali</option>
                    <option value="sitakundo">Sitakundo</option>
                    <option value="chittagong_sadar">Chittagong Sadar</option>
                </select>
            </div>
        
            <div class="form-group">
                <label for="postal_code">Zip/Postal Code:</label>
                <input class="form-control" type="code" name="postal_code" value="" id="postal_code">
            </div>
        
            <div class="form-group">
                <label for="phone number">Phone Number:</label>
                <input class="form-control" type="phone number" name="phone number" value="" id="phone number">
            </div>
        
            <div class="form-group">
                <label for="purchase_order_ref">Purchase Order Ref:</label>
                <input class="form-control" type="number" name="purchase_order_ref" value="" id="purchase_order_ref">
            </div>
        
            <fieldset class="form-group">
                <div class="form-group mt-5">
                    <h1 class="form-text">Shipping Methods</h1>
                    <!-- Default unchecked -->
                    <div class="custom-control custom-radio mt-5">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="defaultExampleRadios">
                        <label class="custom-control-label" for="defaultUnchecked">
                            Free Shopping from BDT 500 and above within Ctg metro only
                        </label>
                    </div>
                </div>
        
                <div class="form-control-lg mt-5 mb-lg-5">
        
                    <a href="payment.php" class="btn btn-success">Review & Payments</a>
                </div>
            </fieldset>
        </form>
    </div>

    
    <!-- Info Link Section -->
    <?php
      include_once('views/frontend/element/infoLinkSec.php');
    ?>

    <!-- Footer Link Section -->
    <?php
      include_once('views/frontend/element/footerLinkSec.php');
    ?>
  
    <!-- Copyright Text -->
    <?php
      include_once('views/frontend/element/copyrightTextSec.php');
    ?>
    
    <!-- Optional JavaScript -->
    <script src="src/js/jquery3.2.1.min.js"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/custom.js"></script>
  </body>
</html>
<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);

$sql = "SELECT * FROM `products`  ";

//Prepare our statement.
$statement = $pdo->prepare($sql);

//Execute the statement and insert our values.
$result = $statement->execute();

$products = $statement->fetchAll();

//$data = $pdo->query("SELECT * FROM  abc")->fetchAll();



$products = $pdo->query("SELECT * FROM `products`  ")->fetchAll();

?>
<!doctype html>
<html lang="en">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="../../src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../src/css/adminPanel.css" type="text/css">

    <title>Admin Panel</title>
</head>
<body>
<?php
include_once('element/header.php');
?>
<div class="container-fluid p-0">
    <div class="text-center">
        <a href="products/create.php" class="btn btn-success m-4">Add New Products</a>
    </div>

    <div class="productTable">
        <table class="table table-bordered text-center">
            <thead class="thead-dark">
            <tr>
                <th id="id">Id</th>
                <th id="title">Title</th>
                <th id="picture">Picture</th>
                <th id="mrp">MRP</th>
                <th id="short_description">Short Description</th>
                <th id="color">Color</th>
                <th id="size">Size</th>
                <th id="created_at">Created At</th>
                <th id="modifies_at">Modified At</th>
                <th id="action">Action</th>
            </tr>
            </thead>
            <?php
            foreach ($products as $row) {
                ?>
                <tbody>
                <tr>
                    <td><?php
                        echo $row['id'];
                        ?></td>
                    <td><?php
                        echo $row['title'];
                        ?></td>
                    <td><?php
                        echo $row['picture'];
                        ?></td>
                    <td><?php
                        echo $row['mrp'];
                        ?></td>
                    <td class="align-middle"><?php
                        echo $row['short_description'];
                        ?></td>
                    <td><?php
                        echo $row['color'];
                        ?></td>
                    <td><?php
                        echo $row['size'];
                        ?></td>
                    <td><?php
                        echo $row['created_at'];
                        ?></td>
                    <td><?php
                        echo $row['modifies_at'];
                        ?></td>
                    <td>
                        <a href="products/view.php?view=<?php
                        echo $row['id'];
                        ?>" class="btn-info rounded px-1"><i
                                    class="fa fa-eye"></i></a>
                        <a href="products/edit.php?edit=<?php
                        echo $row['id'];
                        ?>" class="btn-secondary rounded px-1"><i
                                    class="fa fa-pencil-square-o"></i></a>
                        <a href="products/delete.php?delete=<?php
                        echo $row['id'];
                        ?>" class="btn-danger rounded px-1"><i
                                    class="fa fa-trash"></i></a>
                    </td>
                </tr>
                </tbody>
                <?php
            }
            ?>
        </table>
    </div>
</div>

<!-- Optional JavaScript -->
<script src="../../src/js/jquery3.2.1.min.js"></script>
<script src="../../src/js/bootstrap.min.js"></script>
<script src="../../src/js/adminPanel.js"></script>

</body>

</html>
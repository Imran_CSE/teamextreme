<!doctype html>
<html lang="en">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="../../src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../src/css/adminPanel.css" type="text/css">

    <title>Admin Panel</title>
</head>
<body>
<?php
include_once('element/header.php');
?>

<!-- Optional JavaScript -->
<script src="../../src/js/jquery3.2.1.min.js"></script>
<script src="../../src/js/bootstrap.min.js"></script>
<script src="../../src/js/adminPanel.js"></script>

</body>
</html>
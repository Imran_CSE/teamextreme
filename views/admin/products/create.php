<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="../../../src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../../src/css/adminPanel.css" type="text/css">

    <title>Project by Team Extreme</title>
</head>
<body>
<div class="container">
    <div class="row">
        <form method="post" class="form-horizontal col-md-8 col-md-offset-3" action="store.php">
            <h2> Create Page for Team Extreme </h2>
            <div class="form-group">
                <label for="input1" class="col-sm-2 control-label"> Title</label>
                <div class="col-sm-10">
                    <input type="text" name="title"  class="form-control" id="title" placeholder="varchar" >
                </div>
            </div>


            <div class="form-group">
                <label for="input1" class="col-sm-2 control-label">Picture</label>
                <div class="col-sm-10">
                    <input type="file" name="picture"  class="form-control" id="picture" placeholder="varchar" >
                </div>
            </div>

            <div class="form-group">
                <label for="input1" class="col-sm-2 control-label">MRP</label>
                <div class="col-sm-10">
                    <input type="text" name="mrp"  class="form-control" id="mrp" placeholder="float" >
                </div>
            </div>

            <div class="form-group">
                <label for="input1" class="col-sm-6 control-label"> Short Description</label>
                <div class="col-sm-10">
                    <input type="text" name="short_description"  class="form-control" id="short_description" placeholder="Text Here"  >
                </div>
            </div>

            <div class="form-group">
                <label for="input1" class="col-sm-2 control-label">Color</label>
                <div class="col-sm-10">
                    <input type="text" name="color"  class="form-control" id="color" placeholder="varchar" >
                </div>
            </div>
            <div class="form-group">
                <label for="input1" class="col-sm-2 control-label">Size</label>
                <div class="col-sm-10">
                    <input type="text" name="size"  class="form-control" id="size" placeholder="float">
                </div>
            </div>

            <!--            <div class="form-group">-->
            <!--                <label for="input1" class="col-sm-2 control-label">Phone Number</label>-->
            <!--                <div class="col-sm-10">-->
            <!--                    <input type="text" name="phone"  class="form-control" id="input1" placeholder="--><?php //echo $row['phone']?><!--" />-->
            <!--                </div>-->
            <!--            </div>-->
            <button type="submit" class="btn btn-primary col-md-2 col-md-offset-10" value="submit" />Submit</button>
        </form>
    </div>
</div>

<!-- Optional JavaScript -->
<script src="../../../src/js/jquery3.2.1.min.js"></script>
<script src="../../../src/js/bootstrap.min.js"></script>
<script src="../../../src/js/adminPanel.js"></script>

</body>

</html>
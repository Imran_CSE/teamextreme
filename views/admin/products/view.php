<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);
$id=$_GET['view'];
$sql="SELECT * FROM `products` WHERE id=$id";
$statement=$pdo->prepare($sql);
$edit=$statement->execute();
$view=$statement->fetchAll(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="../../../src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../../src/css/adminPanel.css" type="text/css">

    <title>Project by Team Extreme</title>
</head>
<body>
<div class="container bootstrap snippet">
    <div class="panel-body inf-content mt-5">
        <div class="row">
            <div class="col-md-4 p-4 my-auto">
                <img alt="" style="width:600px;" title="Product Image" class="rounded-circle img-thumbnail" src="../../../src/img/product.png">

            </div>
            <div class="col-md-6">
                <h3 class="my-4">Product Information</h3>
                <div class="table-responsive">
                    <table class="table table-condensed table-responsive table-user-information">
                        <?php
                        foreach ($view as $row) {
                            ?>
                            <tbody>
                            <tr>
                                <td>
                                    <strong>
                                        <i class="fa fa-id-badge"></i>
                                        Identificacion
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['id']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class=""></i>
                                        Title
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['title']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class=""></i>
                                        Picture
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['picture']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class=""></i>
                                        MRP
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['mrp']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class=""></i>
                                        Short Description
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['short_description']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class=""></i>
                                        Color
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['color']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class=""></i>
                                        Size
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['size']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class="fa fa-calendar"></i>
                                        created At
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['created_at']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <i class="fa fa-calendar-plus-o"></i>
                                        Modified At
                                    </strong>
                                </td>
                                <td class="text-primary">
                                    <?php echo $row['modifies_at']?>
                                </td>
                            </tr>
                            </tbody>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <a href="../products.php" class="my-5 btn btn-success">Back To Home</a>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<script src="../../../src/js/jquery3.2.1.min.js"></script>
<script src="../../../src/js/bootstrap.min.js"></script>
<script src="../../../src/js/adminPanel.js"></script>
</body>
</html>

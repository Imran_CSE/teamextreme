<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);
$id=$_GET['edit'];
$sql="SELECT * FROM `products` WHERE id=$id";
$statement=$pdo->prepare($sql);
$edit=$statement->execute();
$editShow=$statement->fetchAll(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="../../../src/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../../../src/css/adminPanel.css" type="text/css">

    <title>Project by Team Extreme</title>
</head>
<body>
    <div class="container">

    <div class="card bg-light">
        <article class="card-body mx-auto" style="max-width: 400px;">
            <h4 class="card-title mt-3 text-center">Edit Account Information</h4>
            <?php
                foreach ($editShow as $row) {
                    ?>
                    <form method="post" action="update.php">
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?php echo $row['id']?>">
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class=""></i>Title: </span>
                            </div>
                            <input name="title" class="form-control" value="<?php echo $row['title']?>" type="varchar">
                        </div> <!-- form-group// -->
                       <!-- <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class=""></i>Picture-> </span>
                            </div>
                            <input name="picture" class="form-control" value="<?php echo $row['picture']?>" type="varchar">
                        </div> -->
                        
                        <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class=""></i>Picture-></span>
                        </div>
                
                    <input type="file" name="picture"  class="form-control" id="picture" placeholder="varchar" >
                </div>
            
                        <!-- form-group// -->
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="f"></i>MRP: </span>
                            </div>

                            <input name="mrp" class="form-control" value="<?php echo $row['mrp']?>" type="float">
                        </div> <!-- form-group// -->

                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class=""></i>Short Description: </span>
                            </div>
                            
                             <input name="short_description" class="form-control" value="<?php echo $row['short_description']?>" type="text">
                        </div> <!-- form-group// -->

                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class=""></i>Color: </span>
                            </div>
                            
                            <input name="color" class="form-control" value="<?php echo $row['color']?>" type="varchar">
                        </div> <!-- form-group// -->

                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class=""></i>Size: </span>
                            </div>
                            
                             <input name="size" class="form-control" value="<?php echo $row['size']?>" type="float">
                        </div> <!-- form-group// -->

                       
                             <!-- form-group// -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"> Save</button>
                        </div> <!-- form-group// -->
                    </form>
                    <?php
                }
            ?>
        </article>
    </div> <!-- card.// -->

</div>

<!-- Optional JavaScript -->
<script src="../../../src/js/jquery3.2.1.min.js"></script>
<script src="../../../src/js/bootstrap.min.js"></script>
<script src="../../../src/js/adminPanel.js"></script>
</body>
</html>

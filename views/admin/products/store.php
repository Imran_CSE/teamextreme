<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);


//Create our INSERT SQL query.
//$sql = "INSERT INTO `cars` (`make`, `model`) VALUES (:make, :model)";

$sql = "INSERT INTO `products` ( `brand_id`, `lebel_id`, `title`, `picture`, `short_description`, `description`, `total_sales`,
`product_type`, `is_new`, `cost`, `mrp`, `special_price`, `soft_delete`, `is_draft`, `is_active`, `created_at`, `modifies_at`,
`color`, `size`)

VALUES ( :brand_id, :lebel_id, :title, :picture, :short_description, :description, :total_sales, :product_type, :is_new, :cost, :mrp,
    :special_price,:soft_delete, :is_draft, :is_active , :created_at,:modifies_at, :color,:size)";

//Prepare our statement.
$statement = $pdo->prepare($sql);

//Bind our values to our parameters (we called them :make and :model).
$statement->bindValue(':brand_id',null);
$statement->bindValue(':lebel_id',null);
$statement->bindValue(':title', $_POST['title']);
$statement->bindValue(':picture', $_POST['picture']);
$statement->bindValue(':short_description', $_POST['short_description']);
$statement->bindValue(':description', null);
$statement->bindValue(':total_sales',null);
$statement->bindValue(':product_type', null);
$statement->bindValue(':is_new', null);
$statement->bindValue(':cost', null);
$statement->bindValue(':mrp', $_POST['mrp']);
$statement->bindValue(':special_price',null);
$statement->bindValue(':soft_delete',null);
$statement->bindValue(':is_draft',null);
$statement->bindValue(':is_active',null);
$statement->bindValue(':created_at', date('Y-m-d h:i:s',time()));
$statement->bindValue(':modifies_at', null);
$statement->bindValue(':color', $_POST['color']);
$statement->bindValue(':size', $_POST['size']);


//Execute the statement and insert our values.
$inserted = $statement->execute();


//Because PDOStatement::execute returns a TRUE or FALSE value,
//we can easily check to see if our insert was successful.
if($inserted){
   header("location:../products.php");
}

?>









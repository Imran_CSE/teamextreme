<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);


$id = $_GET['delete'];
$sql = "DELETE FROM `products` WHERE id=$id";
$statement = $pdo->prepare($sql);
$delete = $statement->execute();


$set = "set @autoid :=0";
$setPrepare = $pdo->prepare($set);
$setExecute = $setPrepare->execute();

$update = "update products set id = @autoid := (@autoid+1)";
$updatePrepare = $pdo->prepare($update);
$updateExecute = $updatePrepare->execute();

$alter = "alter table products Auto_Increment = 1";
$alterPrepare = $pdo->prepare($alter);
$alterExecute = $alterPrepare->execute();



if ($delete) {
    header("location:../products.php");
}
?>

<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database = 'team_extreme';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);

//Connect to MySQL and instantiate our PDO object.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);
$id=$_POST['id'];
$title=$_POST['title'];
$picture=$_POST['picture'];
$mrp=$_POST['mrp'];
$short_description=$_POST['short_description'];
$color=$_POST['color'];
$size=$_POST['size'];
$modifies_at=date('Y-m-d h:i:s',time());

$data = [
    'id' => $id,
    'title' => $title,
    'picture' => $picture,
    'mrp' => $mrp,
    'short_description' => $short_description,
    'color' => $color,
    'size' => $size,
    'modifies_at'=>$modifies_at
];
$sql = "UPDATE products SET title=:title, picture=:picture, mrp=:mrp, short_description=:short_description ,color=:color,size=:size , modifies_at=:modifies_at WHERE id=:id";
$statement= $pdo->prepare($sql);
$update=$statement->execute($data);
if($update){
    header("location:../products.php");
}
?>

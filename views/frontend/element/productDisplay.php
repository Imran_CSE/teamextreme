<div class="productDisplaySec">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="productType text-center">
              <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn">Featured</button>
                <button type="button" class="btn">New Item</button>
                <button type="button" class="btn">Top Seller</button>
                <button type="button" class="btn">Top Ratting</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="singleProduct">
              <div class="card">
                <div class="cardImg">
                  <img class="card-img-top img-fluid" src="src/img/desktop-2.jpg">
                </div>
                <div class="cardContent text-center">
                  <div class="card-body">
                    <h4 class="productTitle">Apple Pc</h4>
                    <p class="price"><del>$1000</del><span>$8000</span></p>
                    <div class="productRating">
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star-half-o"></i></span>
                    </div>
                    <div class="cardHover">
                      <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                      <button class="btn"><i class="fa fa-edit"></i></button>
                      <button class="btn"><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="singleProduct">
              <div class="card">
                <div class="cardImg">
                  <img class="card-img-top img-fluid" src="src/img/mobile-2.jpg">
                </div>
                <div class="cardContent text-center">
                  <div class="card-body">
                    <h4 class="productTitle">Smart Phone Primo V1</h4>
                    <p class="price"><del>$1000</del><span>$8000</span></p>
                    <div class="productRating">
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star-half-o"></i></span>
                    </div>
                    <div class="cardHover">
                      <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                      <button class="btn"><i class="fa fa-edit"></i></button>
                      <button class="btn"><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="singleProduct">
              <div class="card">
                <div class="cardImg">
                  <img class="card-img-top img-fluid" src="src/img/laptop-2.jpg">
                </div>
                <div class="cardContent text-center">
                  <div class="card-body">
                    <h4 class="productTitle">Asus TUF</h4>
                    <p class="price"><del>$1000</del><span>$8000</span></p>
                    <div class="productRating">
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star-half-o"></i></span>
                    </div>
                    <div class="cardHover">
                      <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                      <button class="btn"><i class="fa fa-edit"></i></button>
                      <button class="btn"><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="singleProduct">
              <div class="card">
                <div class="cardImg">
                  <img class="card-img-top img-fluid" src="src/img/mobile-1.jpg">
                </div>
                <div class="cardContent text-center">
                  <div class="card-body">
                    <h4 class="productTitle">Asus ZenPhone</h4>
                    <p class="price"><del>$1000</del><span>$8000</span></p>
                    <div class="productRating">
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star-half-o"></i></span>
                    </div>
                    <div class="cardHover">
                      <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                      <button class="btn"><i class="fa fa-edit"></i></button>
                      <button class="btn"><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="singleProduct">
              <div class="card">
                <div class="cardImg">
                  <img class="card-img-top img-fluid" src="src/img/desktop-1.jpg">
                </div>
                <div class="cardContent text-center">
                  <div class="card-body">
                    <h4 class="productTitle">Dell Brand Pc</h4>
                    <p class="price"><del>$1000</del><span>$8000</span></p>
                    <div class="productRating">
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star-half-o"></i></span>
                    </div>
                    <div class="cardHover">
                      <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                      <button class="btn"><i class="fa fa-edit"></i></button>
                      <button class="btn"><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="singleProduct">
              <div class="card">
                <div class="cardImg">
                  <img class="card-img-top img-fluid" src="src/img/laptop-1.jpg">
                </div>
                <div class="cardContent text-center">
                  <div class="card-body">
                    <h4 class="productTitle">Asus ROG Strix</h4>
                    <p class="price"><del>$1000</del><span>$8000</span></p>
                    <div class="productRating">
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star"></i></span>
                      <span><i class="fa fa-star-half-o"></i></span>
                    </div>
                    <div class="cardHover">
                      <button class="btn"><i class="fa fa-cart"></i>Add to Cart</button>
                      <button class="btn"><i class="fa fa-edit"></i></button>
                      <button class="btn"><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
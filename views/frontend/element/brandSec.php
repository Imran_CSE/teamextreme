<div class="brandSec">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h4 class="text-center">Our Brand</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <div class="card singleBrand">
              <img class="card-img-top img-fluid" src="src/img/brand-1.jpg">
            </div>
          </div>
          <div class="col-md-2">
            <div class="card singleBrand">
              <img class="card-img-top img-fluid" src="src/img/brand-2.jpg">
            </div>
          </div>
          <div class="col-md-2">
            <div class="card singleBrand">
              <img class="card-img-top img-fluid" src="src/img/brand-3.jpg">
            </div>
          </div>
          <div class="col-md-2">
            <div class="card singleBrand">
              <img class="card-img-top img-fluid" src="src/img/brand-4.jpg">
            </div>
          </div>
          <div class="col-md-2">
            <div class="card singleBrand">
              <img class="card-img-top img-fluid" src="src/img/brand-5.jpg">
            </div>
          </div>
          <div class="col-md-2">
            <div class="card singleBrand">
              <img class="card-img-top img-fluid" src="src/img/brand-6.jpg">
            </div>
          </div>
        </div>
      </div>
    </div>
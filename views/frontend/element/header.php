<div class="headerSec">
      <div class="topNav">
        <nav class="navbar-expand-sm">
          <div class="container navbar-collapse">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php"><i class="fa fa-home"></i> Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-user"></i> My Account</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shopping Cart</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-check"></i> Checkout</a>
              </li>
            </ul>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <select class="custom-select">
                  <option>US</option>
                  <option>UK</option>
                  <option>Bangladesh</option>
                  <option>India</option>
                  <option>Pakistan</option>
                </select>
              </li>
              <li class="nav-item">
                <select class="custom-select">
                  <option><i class="fa fa-dollar"></i>Dollar</option>
                  <option><i class="fa fa-"></i>Taka</option>
                  <option><i class="fa fa-euro"></i>Euro</option>
                  <option><i class="fa fa-pound-sign"></i>Pound</option>
                  <option><i class="fa fa-rupee"></i>Rupee</option>
                </select>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="midNav">
        <nav class="navbar navbar-expand-sm">
          <div class="container">
            <a class="navbar-brand" href="index.php">
              <h3 class="p-1">Team Extreme</h3>
            </a>
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a class="nav-link" href="#">Login</a></li>
              <li class="nav-item"><a class="nav-link" href="#">Creat Account</a></li>
              <li class="nav-item"><a class="nav-link" href="#">
                <i class="fa fa-edit"></i>
              </a></li>
              <li class="nav-item"><a class="nav-link" href="#">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
              </a></li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="menuBar">
        <nav class="navbar navbar-expand-sm">
          <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myMenu" aria-controls="myMenu" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="myMenu">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link active" href="all_product.php">All Product <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Laptop</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Mobile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Tablet</a>
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn my-2 my-sm-0" type="submit">Search</button>
              </form>
            </div>
          </div>
        </nav>
      </div>
    </div>
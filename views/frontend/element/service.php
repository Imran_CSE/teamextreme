<div class="servicesSec">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 serviceItem">
            <div class="card">
              <div class="card-block card-1">
                <h3 class="card-title">Special title</h3>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" title="Read more" class="read-more" >Read more <i class="fa fa-angle-double-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 serviceItem">
            <div class="card">
              <div class="card-block card-2">
                <h3 class="card-title">Special title</h3>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" title="Read more" class="read-more" >Read more <i class="fa fa-angle-double-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 serviceItem">
            <div class="card">
              <div class="card-block card-3">
                <h3 class="card-title">Special title</h3>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" title="Read more">Read more <i class="fa fa-angle-double-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 serviceItem">
            <div class="card">
              <div class="card-block card-4">
                <h3 class="card-title">Special title</h3>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" title="Read more" class="read-more" >Read more <i class="fa fa-angle-double-right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
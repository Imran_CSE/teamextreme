<div class="footerLinkSec">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group flex-lg-row flex-md-coloumn text-center">
              <li class="list-group-item">We Accept</li>
              <li class="list-group-item"><img class="img-fluid" src="src/img/card-1.jpg"></li>
              <li class="list-group-item"><img class="img-fluid" src="src/img/card-2.jpg"></li>
              <li class="list-group-item"><img class="img-fluid" src="src/img/card-3.jpg"></li>
              <li class="list-group-item"><img class="img-fluid" src="src/img/card-4.jpg"></li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul class="list-group flex-lg-row flex-md-coloumn text-center">
              <li class="list-group-item">Follow Us</li>
              <a class="list-group-item" href="#"><i class="fa fa-facebook"></i></a>
              <a class="list-group-item" href="#"><i class="fa fa-twitter"></i></a>
              <a class="list-group-item" href="#"><i class="fa fa-google-plus"></i></a>
              <a class="list-group-item" href="#"><i class="fa fa-behance"></i></a>
              <a class="list-group-item" href="#"><i class="fa fa-dribbble"></i></a>
            </ul>
          </div>
        </div>
      </div>
    </div>
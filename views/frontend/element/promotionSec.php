<div class="promotionSec">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="promoImg">
              <img src="src/img/offerImg.jpg" alt="Promotion Image" class="img-fluid">
              <div class="promoPrice rounded-circle">
                <p><del class="d-block align-middle">$1000</del>
                <span>$500</span></p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="promoContent">
              <h2 class="promoTitle">50% OFF<small class="d-block">For walton primo</small></h2>
              <span>Lorem ipsum dolor sit amet.</span>
              <p class="text-justify">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum dolores animi rem velit facere eius maxime adipisci, corporis dolore odio.
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum dolores animi rem velit facere eius maxime adipisci, corporis dolore odio.
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum dolores animi rem velit facere eius maxime adipisci, corporis dolore odio.
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum dolores animi rem velit facere eius maxime adipisci, corporis dolore odio.
              </p>
              <div class="promoTime">
                <ol>
                  <li class="btn"><span class="d-block" id="showDays"></span>Days</li>
                  <li class="btn"><span class="d-block" id="showHours"></span>Hours</li>
                  <li class="btn"><span class="d-block" id="showMinutes"></span>Minutes</li>
                  <li class="btn"><span class="d-block" id="showSeconds"></span>Seconds</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
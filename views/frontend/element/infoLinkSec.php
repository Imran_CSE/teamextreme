<div class="infoLinkSec">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div>
              <h4>Information</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> About Us</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> CUSTOMAR SERVICE</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> PRIVACY POLICY</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> Site Map</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> Contact</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div>
                <h4>Why Buy from Us?</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> About Us</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> CUSTOMAR SERVICE</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> PRIVACY POLICY</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> Site Map</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> Contact</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div>
                <h4>My Account</h4>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> About Us</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> CUSTOMAR SERVICE</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> PRIVACY POLICY</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> Site Map</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-caret-right"></i> Contact</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="contactInfo">
              <h4>Contact</h4>
              <div>
                <i class="fa fa-map-marker"></i>
                <p>21 Revolution Street<br> Paris, France</p>
              </div>
      
              <div>
                <i class="fa fa-phone"></i>
                <p>+1 555 123456</p>
              </div>
      
              <div>
                <i class="fa fa-envelope"></i>
                <p><a href="mailto:support@company.com">support@company.com</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
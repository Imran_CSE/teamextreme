<div class="subscriptionSec">
      <div class="container">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <h2 class="subcriptionTitle text-center">Subcribe for Get Offer</h2>
            <p class="text-center">Lorem ipsum dolor sit amet.</p>
            <div class="input-group">
              <input type="email" name="subEmail" class="form-control" placeholder="Type your Email">
              <div class="input-group-append">
                <button class="btn" type="submit">Let's Go!</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>